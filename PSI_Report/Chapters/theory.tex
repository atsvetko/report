\chapter{Theoretical Aspects}
\label{ch:Theory}
This chapter will describe the theoretical foundation of the experiment, i.e. the particles involved, the \Ppizero production mechanism, the Dalitz decay and the possible scenarios for experimentally measuring the decay.
\section{Involved Particles}
\label{ch:Theory:sec:Particles}

\subsection{\Ppizero}
The neutral pion \Ppizero is the lightest hadron and more specifically, the lightest meson at \SI{134.9766(6)}{\mega\electronvolt} \cite{PDG}. 
It is a superposition of two up and down quarks (\HepProcess{\Pup\APup} and \HepProcess{\Pdown\APdown}) and has a mean lifetime of around \SI{8.4e-17}{\second}, which is shorter than the charged pion lifetime by nine orders of magnitude. This is due to the fact that charged pions decay via the weak force, whereas neutral pions decay via the electromagnetic force. The dominating decay mode for the \Ppizero with a branching ratio of \SI{98.823}{\percent}\cite{PDG} is the decay into two photons:
\begin{equation}
\HepProcess{\Ppizero \to \Pgamma \Pgamma}.
\end{equation}
\label{pp}
The next likeliest decay is the Dalitz decay 
\begin{equation}
\HepProcess{\Ppizero\to\Pgamma\Pelectron\APelectron}.
\end{equation}
with a literature value for the branching ratio of \SI{1.174}{\percent}. In this case, one of the two photons undergoes an internal conversion to an \HepProcess{\Pelectron\APelectron} pair, which is why this decay is suppressed by a factor of approximately $\frac{2}{\alpha}$ compared to \eqref{pp}. Measuring this branching ratio is the goal of the experiment described in this report.\\ The decay mode where both photons are subject to an internal conversion is called the double Dalitz decay, with a branching ratio of \SI{3.34e-3}{\percent}:
\begin{equation}
\HepProcess{\Ppizero\to\Pelectron\APelectron\Pelectron\APelectron}.
\end{equation}
The remaining decay mode 
\begin{equation}
\HepProcess{\Ppizero\to\Pelectron\APelectron} 
\end{equation}
is strongly suppressed ($B=\SI{6.46e-6}{\percent}$), due to the helicity conservation of the electromagnetic interaction and $m_e\approx0$.

\subsection{\Pgamma}
The photon is a zero-rest-mass, zero-charge bosonic elementary particle and the quantum of the electromagnetic field, hereby mediating the electromagnetic force. It is part of most \Ppizero decay modes (see above) and therefore needs to be detected in the experiment. Making use of the interaction of photons with matter, it can be detected using a sandwiched scintillator-copper-scintillator setup: if a photon passes through this setup, it will not trigger the first scintillator but cause the emission of charged particles via the photoelectric effect or pair production in the copper plate, which will then be detected in the second scintillator. 

\subsection{\Pelectron \APelectron}
The electron \Pelectron and positron \APelectron are leptonic elementary particles with positive and negative elementary electric charge, respectively, and a rest mass of around \SI{511}{\kilo\electronvolt}. The Dalitz decay features an \HepProcess{\Pelectron\APelectron} pair and the experimental setup therefore needs to be able to detect them. This can be done using a scintillator and a calorimeter. 

\section{Dalitz Decay}
\label{ch:Theory:sec:Dalitz}
\subsection{Kinematics}
In the process %\HepProcess{\Ppizero\to\Pphoton\Pphoton}
 the momenta of the two photons are anti-parallel in the CMS due to momentum conservation. If now one of the photons internally converts into an electron-positron pair these two particles will move approximately parallel to the photon direction, since the electron mass is negligible and the 4-momentum has to be conserved. In the lab frame the decay products are boosted due the \Ppizero momentum, so that the opening angle between the photon and the electron-positron pair is $\alpha<\SI{180}{\degree}$.\\ Let the z-axis be parallel to the direction of the \Ppizero and $\theta^*$ be the angle between the photon and z-direction. Then the photon momentum in the rest system of the \Ppizero is
\begin{equation}
p_{\gamma}^*=\frac{1}{2}m_{\Ppizero}\begin{pmatrix}1 \\ \sin(\theta^*) \\ 0 \\\cos(\theta^*)\end{pmatrix}
\end{equation}
and 
\begin{equation}
p_{\gamma}=\Lambda p_{\gamma}^*
\end{equation}\label{boost}
in the lab system, where $\Lambda$ is the boost matrix
\begin{equation}
\Lambda=\begin{pmatrix}
\gamma & 0 & 0 & \gamma\beta\\
0 & 1 & 0 & 0\\
0 & 0 & 1 & 0\\
\gamma\beta & 0 & 0 & \gamma\\
\end{pmatrix}.
\end{equation} 
From the momentum given in \eqref{boost} one can calculate the 
photon energy 
\begin{equation}
E^{\gamma}=\gamma \frac{1}{2} m_{\Ppizero}(1+\beta\cos(\theta^*))\\
E^{\gamma}_{\text{min/max}}=\gamma \frac{1}{2} m_{\Ppizero}(1\pm\beta)
\end{equation}
and the opening angle $\alpha$ in the lab frame. The larger the opening angle $\alpha$, i.e. the smaller $\theta^*$, the smaller the corresponding phase space: $\theta^*=\frac{\pi}{2}$ describes a full circle while $\theta^*=0$ corresponds to only one point. Therefore, the  most likely opening angle is the minimal opening angle $\alpha_{min}$ at $\theta^*=\frac{\pi}{2}$.



\section{Pion Production}
The experiment is provided a beam of charged pions courtesy of PSI. Neutral pions can be produced via the charge exchange reactions
\begin{equation}
\HepProcess{\Ppiplus \Pneutron \to \Ppizero \Pproton}
\end{equation}
and 
\begin{equation}
\HepProcess{\Ppiminus \Pproton \to \Ppizero \Pneutron}.
\label{scA}
\end{equation}
The produced \Ppizero will decay immediately after production due to their short lifetime. 

Consider a hydrogen target enabling the second reaction in order to calculate the energy of the produced \Ppizero. The rest masses of the \Pproton and \Ppiminus are $m_{\Pproton}=\SI{938.3}{\mega\electronvolt}$ and $m_{\Ppiminus}=\SI{139.6}{\mega\electronvolt}$, totalling $\SI{1077.9}{\mega\electronvolt}$. The rest masses of the \Pneutron and \Ppizero are $m_{\Pneutron}=\SI{939.6}{\mega\electronvolt}$ and $m_{\Ppizero}=\SI{135}{\mega\electronvolt}$, totalling $\SI{1074.6}{\mega\electronvolt}$. Subtracting both values yields a difference of $Q=\SI{3.3}{\mega\electronvolt}$. The center-of-mass (CMS) energy $\sqrt{s}$ of a two-particle system is 
\begin{equation}
\sqrt{s}=m_1\sqrt{1+\frac{p^2}{m_1^2}} + m_2\sqrt{1+\frac{p^2}{m_2^2}}
\label{s}
\end{equation}
where $m_1$ and $m_2$ are the rest masses of the two particles. With $p \ll m$ this gives
\begin{equation}
\sqrt{s}\approx m_1\left(1+\frac{p^2}{2m_1^2}\right) + m_2\left(1+\frac{p^2}{2m_2^2}\right)
\end{equation}
and thus
\begin{equation}
p^2=2(\sqrt{s} - m_1 - m_2)\bar m\approx 2 Q \bar m 
\end{equation}
where
\begin{equation}
\bar{m} = \frac{m_1m_2}{m_1+m_2}.
\end{equation}

Inserting the \Ppizero and \Pneutron mass, this yields a CMS-momentum for the \Ppizero of $p_A=\SI{27.9}{\mega\electronvolt}$. The decay products of the pion are therefore boosted.

Neutral pion production may also occur via the \PDelta-resonance (as in scenario B of the next section):
\begin{equation}
\HepProcess{\Ppiminus \Pproton\to\HepParticleResonance{\PDelta}{1232}{}{}\to\Ppizero\Pneutron}.
\label{scB}
\end{equation}
With $p_{\text{beam}}=\sqrt[]{E_{\text{beam}}^2-m_{\Ppiminus}^2}$ and $\sqrt[]{s}=\SI{1232}{\mega\electronvolt}$ we find that the resonant CMS-energy 
\begin{equation}
s=\left(\begin{pmatrix}E_{\text{beam}}\\\vec{p}_{\text{beam}}\end{pmatrix}+\begin{pmatrix}m_P\\\vec{0}\end{pmatrix}\right)^2=m_P^2+m_{\Ppiminus}^2+2m_PE_{\text{beam}}
\end{equation}
is equivalent to a \Ppiminus-beam energy of $E_{\text{beam}}\approx\SI{300}{\mega\electronvolt}$. 
Rearranging \eqref{s} gives
\begin{equation}
p=\sqrt[]{\frac{s^2-m_1^2-m_2^2-4m_1^2m_2^2}{4s}}.
\end{equation}
Thus, the CMS-momentum of the \Ppizero in this scenario is $p_B^*\approx\SI{207}{\mega\electronvolt}$. Because of the second boost from the process %\HepProcess{\Ppiminus \Pproton\to\HepParticleResonance{\PDelta}{1232}{}{}}
  this corresponds to $\SI{157}{\mega\electronvolt}\leq p_B\leq\SI{266}{\mega\electronvolt}$ in the lab system, depending on the angle of the \Ppizero to the \Ppiminus beam.


\subsection{Scenarios A\&B}
\label{ch:Theory:sec:Scenarios}
There are two ways by which we can produce the \Ppizero, given in \eqref{scA} and \eqref{scB}.
We insert the \Ppizero momenta $p_{A/B}$ calculated above to find the gamma-factor $\gamma=\frac{E}{m}$ and velocity $\beta=\frac{p}{E}$ for both scenarios. This gives the minimal opening angles using \eqref{boost}
\begin{equation}
\alpha^{\text{min}}_A=\SI{156}{\degree}
\end{equation}
\begin{equation}
\bar\alpha^{\text{min}}_B=\SI{66}{\degree},
\end{equation}
where $\alpha^{\text{min}}$ is averaged over the angle of the \Ppizero to the beam in scenario B.
Since the minimal opening angle is much smaller in scenario B than in scenario A, the detectors are positioned closer to the beam which increases the background. 
However, in scenario A the CH$_2$ target has to be thick enough to stop the \Ppiminus. This leads to more background from external conversions to \Ppositron\Pelectron of one of the photons in %\HepProcess{\Ppizero\to\Pphoton\Pphoton}
: The Dalitz decay rate is proportional to the target thickness t while the background from photon conversions in the target material is proportional to the thickness squared.
\begin{align*}
R_{\text{Dalitz}}=r\Omega B \\
R_{\text{conversion}}=r\Omega kt
\end{align*}
Here, $\Omega$ is the solid angle, k the conversion probability and r the \Ppizero production rate. It is given by $r=\sigma t\rho n$ with  the target density $\rho$, the \Ppiminus rate n and the cross-section $\sigma$.
Therefore, a thin target is of advantage, which is why we chose scenario B for our the experiment.