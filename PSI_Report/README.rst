How to compile
==============
.. code:: bash

  pdflatex report.tex
  bibtex report
  pdflatex report.tex
  pdflatex report.tex

Work with it
============
In the Main-Part section in the report.tex include the path to the .tex file. The files should be in the folder chapters. Figures and plots should be put into the figures folder. The literature should be put into the lit.bib file. New packages or commands can be added in the report.tex.