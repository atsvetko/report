\select@language {english}
\contentsline {section}{\numberline {1}Involved Particles}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}CHANGEME}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}CHANGEME}{3}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}CHANGEMECHANGEME}{4}{subsection.1.3}
\contentsline {section}{\numberline {2}Dalitz Decay}{4}{section.2}
\contentsline {subsection}{\numberline {2.1}Kinematics}{4}{subsection.2.1}
\contentsline {section}{\numberline {3}Pion Production}{5}{section.3}
\contentsline {subsection}{\numberline {3.1}Scenarios A\&B}{6}{subsection.3.1}
\contentsline {section}{\numberline {4}Experimental Setup}{8}{section.4}
\contentsline {subsection}{\numberline {4.1}Setup}{8}{subsection.4.1}
\contentsline {subsection}{\numberline {4.2}beam and Area}{8}{subsection.4.2}
\contentsline {subsection}{\numberline {4.3}Components}{8}{subsection.4.3}
\contentsline {subsection}{\numberline {4.4}Arms (find new name)}{8}{subsection.4.4}
\contentsline {subsection}{\numberline {4.5}DAQ}{8}{subsection.4.5}
\contentsline {subsubsection}{\numberline {4.5.1}GUI of the DAQ}{9}{subsubsection.4.5.1}
\contentsline {paragraph}{1)}{9}{section*.2}
\contentsline {paragraph}{2)}{9}{section*.3}
\contentsline {paragraph}{3)}{9}{section*.4}
\contentsline {paragraph}{4)}{9}{section*.5}
\contentsline {paragraph}{5)}{9}{section*.6}
\contentsline {paragraph}{6)}{10}{section*.7}
\contentsline {paragraph}{7)}{10}{section*.8}
\contentsline {paragraph}{8)}{10}{section*.9}
\contentsline {paragraph}{9)}{10}{section*.10}
\contentsline {paragraph}{10)}{10}{section*.11}
\contentsline {subsubsection}{\numberline {4.5.2}Data taking from the logic boxes}{10}{subsubsection.4.5.2}
\contentsline {subsubsection}{\numberline {4.5.3}Converting and writting of the data}{11}{subsubsection.4.5.3}
\contentsline {subsection}{\numberline {4.6}Trigger Logic}{12}{subsection.4.6}
\contentsline {subsubsection}{\numberline {4.6.1}Logic-Box input}{12}{subsubsection.4.6.1}
\contentsline {subsubsection}{\numberline {4.6.2}Generating of the trigger-signal}{12}{subsubsection.4.6.2}
\contentsline {subsection}{\numberline {4.7}Calibration}{12}{subsection.4.7}
\contentsline {chapter}{Bibliography}{14}{section*.12}
